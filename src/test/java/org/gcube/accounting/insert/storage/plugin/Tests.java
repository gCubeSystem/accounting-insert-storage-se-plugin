package org.gcube.accounting.insert.storage.plugin;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Tests {

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(Tests.class);

	@Test
	public void testLaunch() throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		AccountingInsertStoragePlugin plugin = new AccountingInsertStoragePlugin();
		logger.debug("Going to launch {}", plugin.getClass().getSimpleName());
		plugin.launch(inputs);
	}

}
