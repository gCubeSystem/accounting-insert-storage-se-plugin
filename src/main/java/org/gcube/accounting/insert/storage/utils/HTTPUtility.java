package org.gcube.accounting.insert.storage.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HTTPUtility {
	
	private static final Logger logger = LoggerFactory.getLogger(HTTPUtility.class);
	
	public static StringBuilder getStringBuilder(InputStream inputStream) throws IOException {
		StringBuilder result = new StringBuilder();
		try(BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
			String line;
			while((line = reader.readLine()) != null) {
				result.append(line);
			}
		}
		
		return result;
	}
	
	public static String getResultAsString(HttpURLConnection httpURLConnection) throws IOException {
		int responseCode = httpURLConnection.getResponseCode();
		if(responseCode >= Status.BAD_REQUEST.getStatusCode()) {
			Status status = Status.fromStatusCode(responseCode);
			InputStream inputStream = httpURLConnection.getErrorStream();
			StringBuilder result = getStringBuilder(inputStream);
			logger.trace(result.toString());
			throw new WebApplicationException(result.toString(), status);
		}
		InputStream inputStream = httpURLConnection.getInputStream();
		String ret = getStringBuilder(inputStream).toString();
		logger.trace("Got Respose is {}", ret);
		return ret;
	}
	
}
