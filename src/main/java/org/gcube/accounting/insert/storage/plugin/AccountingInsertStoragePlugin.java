
package org.gcube.accounting.insert.storage.plugin;

import java.net.HttpURLConnection;
import java.net.URI;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.gcube.accounting.datamodel.UsageRecord.OperationResult;
import org.gcube.accounting.datamodel.basetypes.AbstractStorageUsageRecord.DataType;
import org.gcube.accounting.datamodel.usagerecords.StorageStatusRecord;
import org.gcube.accounting.insert.storage.utils.HTTPUtility;
import org.gcube.accounting.insert.storage.utils.SocialService;
import org.gcube.accounting.persistence.AccountingPersistence;
import org.gcube.accounting.persistence.AccountingPersistenceFactory;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.common.gxhttp.request.GXHTTPStringRequest;
import org.gcube.contentmanagement.blobstorage.service.IClient;
import org.gcube.contentmanager.storageclient.wrapper.AccessType;
import org.gcube.contentmanager.storageclient.wrapper.MemoryType;
import org.gcube.contentmanager.storageclient.wrapper.StorageClient;
import org.gcube.vremanagement.executor.plugin.Plugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI-CNR)
 * @author Alessandro Pieve (ISTI - CNR)
 */
public class AccountingInsertStoragePlugin extends Plugin {

	private static Logger logger = LoggerFactory.getLogger(AccountingInsertStoragePlugin.class);

	protected static final String ALL_USERS_PATH = "2/users/get-all-usernames";
	protected static final String RESPONSE_SUCCESS_KEY = "success";
	protected static final String RESPONSE_MESSAGE_KEY = "message";
	protected static final String RESULT_KEY = "result";

	protected ObjectMapper objectMapper;

	public AccountingInsertStoragePlugin() {
		super();
		objectMapper = new ObjectMapper();
	}

	/** {@inheritDoc} */
	@Override
	public void launch(Map<String, Object> inputs) throws Exception {

		AccountingPersistence accountingPersistence = AccountingPersistenceFactory.getPersistence();
		
		Set<String> users = getAllUsers();

		for (String consumerId : users) {
			// for each user call storage manager and insert
			IClient client = new StorageClient("", "", consumerId, AccessType.PUBLIC, MemoryType.PERSISTENT)
					.getClient();
			StorageStatusRecord storageStatusRecord = new StorageStatusRecord();
			try {

				Long dataVolume = Long.parseLong(client.getTotalUserVolume());
				Long dataCount = Long.parseLong(client.getUserTotalItems());

				storageStatusRecord.setConsumerId(consumerId);
				storageStatusRecord.setDataVolume(dataVolume);
				storageStatusRecord.setDataCount(dataCount);
				storageStatusRecord.setDataType(DataType.STORAGE);
				storageStatusRecord.setOperationResult(OperationResult.SUCCESS);
				storageStatusRecord.setProviderURI(new URI("data.d4science.org"));

				accountingPersistence.account(storageStatusRecord);
				
			} catch (Exception e) {
				logger.error("Error while accounting {} for {} for user {}", storageStatusRecord.getRecordType(),
						DataType.STORAGE, consumerId);
			}

		}
	}

	private Set<String> getAllUsers() throws Exception {
		String basePath = SocialService.getSocialService().getServiceBasePath();
		if (basePath == null) {
			logger.error("Unable to get users because there is no social networking service available");
			throw new Exception("Unable to get users because there is no social networking service available");
		}
		basePath = basePath.endsWith("/") ? basePath : basePath + "/";

		GXHTTPStringRequest gxhttpStringRequest = GXHTTPStringRequest.newRequest(basePath);
		gxhttpStringRequest.from(AccountingInsertStoragePlugin.class.getSimpleName());
		gxhttpStringRequest.path(ALL_USERS_PATH);
		HttpURLConnection httpURLConnection = gxhttpStringRequest.get();
		String ret = HTTPUtility.getResultAsString(httpURLConnection);

		JsonNode jsonNode = objectMapper.readTree(ret);
		if (!jsonNode.get(RESPONSE_SUCCESS_KEY).asBoolean()) {
			logger.info("Failed to get users. Reason {}", jsonNode.get(RESPONSE_MESSAGE_KEY).asText());
		}

		 ArrayNode arrayNode = (ArrayNode) jsonNode.get(RESULT_KEY);
		 Set<String> users = new HashSet<>(arrayNode.size());
		 for(int i=0; i<arrayNode.size(); i++) {
			 users.add(arrayNode.get(i).asText());
		 }
		 
		 return users;
	}

	/** {@inheritDoc} */
	@Override
	protected void onStop() throws Exception {
		logger.trace("AccountingInsertStoragePlugin: {} onStop() function", this.getClass().getSimpleName());
		Thread.currentThread().interrupt();
	}

}
