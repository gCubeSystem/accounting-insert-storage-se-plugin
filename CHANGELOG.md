This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Accounting Insert Storage Smart Executor Plugin


## [v2.0.0] 

- Ported plugin to smart-executor APIs 3.0.0 [#21368]
- The plugin has been completely rewrote

## [v1.0.1]

- Fixed pom for java 8


## [v1.0.0] 

- First Release

